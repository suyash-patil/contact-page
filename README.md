This is a contact page which uses DOM manipulation and JavaScript localStorage. It will store user data and will show that on home page. If user doesn't have filled form then it will show contact form link. Here are some screenshots:

![](images/contact-page-with-info.png)

![](images/contact-form.png)

Ensure to change the `window.location.pathname = "/F:/contact-page/index.html"` on line 18 in script.js to the pathname of index.html on your system.
