const details = localStorage.length;

if(details){
  const btn = document.getElementById('contact-btn');
  const no_info = document.getElementById('no-info');
  document.getElementById('first_name').innerHTML = localStorage.getItem('fullname');
  document.getElementById('last_name').innerHTML = localStorage.getItem('lastname');
  document.getElementById('email_id').innerHTML = localStorage.getItem('emailid');
  document.getElementById('phone_number').innerHTML = localStorage.getItem('phoneNumber');
  no_info.style.display = "none";
  btn.style.display = "none";
}
else {
  const text = document.getElementById('alrdy-cnt');
  const info = document.getElementById('cnt-info');
  info.style.display = "none";
  text.style.display = "none";
}
